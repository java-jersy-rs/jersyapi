package jersy.first;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.sun.xml.internal.ws.spi.db.DatabindingException;

import jersy.exception.DataNotFoundException;
import jersy.model.Person;

/*
 * http://localhost:8080/JavaApi/person
 */
@Path("/person")
public class PersonController {

	private static List<Person> allPerson = new ArrayList<Person>();
	private Map<String, Object> resultResponse;

	@Context
	Request request;

	@Context
	UriInfo uriInfo;

	static {
		allPerson.add(new Person(1, "Monika", "Female", "015247661"));
		allPerson.add(new Person(2, "Noraco", "Male", "015247662"));
		allPerson.add(new Person(3, "Kavcha", "Female", "015247663"));
		allPerson.add(new Person(4, "Kachoa", "Male", "015247664"));
		allPerson.add(new Person(5, "Meanab", "Female", "015247665"));
		allPerson.add(new Person(6, "Voices", "Male", "015247666"));
		allPerson.add(new Person(7, "Servic", "Female", "015247667"));
		allPerson.add(new Person(8, "Bolasi", "Male", "015247668"));
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllPerson() {
		resultResponse = new HashMap<String, Object>();
		resultResponse.put("CODE", "0000");
		resultResponse.put("MSGA", "Sucess");
		resultResponse.put("DATA", allPerson);
		return Response.status(Response.Status.OK).entity(resultResponse).build();
	}

	@Path("{id}")
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response getPerson(@PathParam("id") Integer id) {
		Person person = null;
		for (Person p : allPerson) {
			if (p.getId() == id) {
				person = p;
				break;
			}
		}		
		if (person == null) {
			throw new DataNotFoundException("Person is null");
		}		
		resultResponse = new HashMap<String, Object>();
		resultResponse.put("CODE", "0000");
		resultResponse.put("MSGA", "Sucess");
		resultResponse.put("DATA", person);
		//Link self = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()).rel("self").build();		
		return Response.status(Response.Status.OK).entity(resultResponse).build();
	}

	/**
	 * Request Json Body base on JsonProperties field name { "ID": 6, "NAME":
	 * "Coca", "GENDER": "male", "TEL": "015247663" }
	 * 
	 * @param person
	 * @return
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response savePerson(Person person) {
		if (person != null) {
			allPerson.add(person);
		} else {
			throw new DataNotFoundException("Error Request Data");
		}
		resultResponse = new HashMap<String, Object>();
		resultResponse.put("CODE", "0000");
		resultResponse.put("MSGA", "Sucess");
		resultResponse.put("DATA", person);
		return Response.status(Response.Status.OK).entity(resultResponse).build();
	}

	/**
	 * Delete Person by PathParam id
	 * 
	 * @param id
	 * @return
	 */
	@Path("{id}")
	@DELETE
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response deletePerson(@PathParam("id") Integer id) {
		Person person = null;
		for (Person p : allPerson) {
			if (p.getId() == id) {
				person = p;
				break;
			}
		}
		allPerson.remove(person);
		resultResponse = new HashMap<String, Object>();
		resultResponse.put("CODE", "0000");
		resultResponse.put("MSGA", "Sucess");
		resultResponse.put("DATA", person);
		return Response.status(Response.Status.OK).entity(resultResponse).build();
	}

	/**
	 * Modify Person by id in object Person { "ID": 1, "NAME": "Coca", "GENDER":
	 * "male", "TEL": "015247663" }
	 * 
	 * @param person
	 * @return
	 */
	@PUT
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response modifyPerson(Person person) {
		for (int i = 0; i < allPerson.size(); i++) {
			if (allPerson.get(i).getId() == person.getId()) {
				allPerson.get(i).setName(person.getName());
				allPerson.get(i).setGender(person.getGender());
				allPerson.get(i).setTel(person.getTel());
				break;
			}
		}
		allPerson.remove(person);
		resultResponse = new HashMap<String, Object>();
		resultResponse.put("CODE", "0000");
		resultResponse.put("MSGA", "Sucess");
		resultResponse.put("DATA", person);
		return Response.status(Response.Status.OK).entity(resultResponse).build();
	}

	/**
	 * http://localhost:8080/JavaApi/person/param?id=200&name=thatt%20thonn
	 * 
	 * @param id
	 * @param name
	 * @return
	 */
	@Path("/param")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPersonByQueryParam(@DefaultValue("1") @QueryParam("id") Integer id,
			@DefaultValue("Momo") @QueryParam("name") String name) {
		resultResponse = new HashMap<String, Object>();
		resultResponse.put("CODE", "0000");
		resultResponse.put("MSGA", "Sucess");
		resultResponse.put("ID", id);
		resultResponse.put("name", name);
		return Response.status(Response.Status.OK).entity(resultResponse).build();
	}

}
