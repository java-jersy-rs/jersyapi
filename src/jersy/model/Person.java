package jersy.model;

import javax.json.bind.annotation.JsonbProperty;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Person {

	@JsonbProperty("ID")
	private Integer id;
	
	@JsonbProperty("NAME")
	private String name;
	
	@JsonbProperty("GENDER")
	private String gender;
	
	@JsonbProperty("TEL")
	private String tel;
	
	
	public Person() {
		// TODO Auto-generated constructor stub
	}
	
	public Person(Integer id, String name, String gender, String tel) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.tel = tel;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}	
	
}
