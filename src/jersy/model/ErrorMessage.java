package jersy.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ErrorMessage {

	private String errorMesage;
	private String errorCode;
	private String documentation;

	public ErrorMessage() {
		// TODO Auto-generated constructor stub
	}

	public ErrorMessage(String errorMesage, String errorCode, String documentation) {
		super();
		this.errorMesage = errorMesage;
		this.errorCode = errorCode;
		this.documentation = documentation;
	}

	public String getErrorMesage() {
		return errorMesage;
	}

	public void setErrorMesage(String errorMesage) {
		this.errorMesage = errorMesage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getDocumentation() {
		return documentation;
	}

	public void setDocumentation(String documentation) {
		this.documentation = documentation;
	}	
	
	

}
