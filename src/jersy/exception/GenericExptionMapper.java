package jersy.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import jersy.model.ErrorMessage;

@Provider
public class GenericExptionMapper implements ExceptionMapper<Throwable> {

	@Override
	public Response toResponse(Throwable ex) {		
		ErrorMessage errorMessage = new ErrorMessage(ex.getMessage(), "500", "http://localhost:8080/");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(errorMessage).build();
	}
		

}
